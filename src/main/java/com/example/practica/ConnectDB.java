package com.example.practica;

import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

public class ConnectDB {

    private Connection dbConn = null;

    private Connection getDBConn() throws ClassNotFoundException, SQLException {
        Map<String, String> env = System.getenv();
        String host = env.getOrDefault("DB_HOST", "localhost");
        String port = env.getOrDefault("DB_PORT", "3306");
        String database = env.getOrDefault("DB_NAME", "dem");
        String user = env.getOrDefault("DB_USER", "root");
        String password = env.getOrDefault("DB_HOST", "");

        String connectionURL = String.format("jdbc:mysql://%s:%s/%s?serverTimezone=UTC",

                host,
                port,
                database);

        Class.forName("com.mysql.cj.jdbc.Driver");

        if (dbConn == null){
            dbConn = DriverManager.getConnection(
                    connectionURL,
                    user,
                    password
            );
        }

        return dbConn;
    }

    public Integer getSotrydnic(String number) throws SQLException, ClassNotFoundException {
        String sql ="SELECT count(*) FROM sotrudniki WHERE telephon='"+number+"';";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        Integer vxod = 0;
        while (result.next()){
            vxod = result.getInt(1);
        }
        return vxod;
    }


    public String getPassword(String number) throws SQLException, ClassNotFoundException {
        String sql ="SELECT sotrudniki.password FROM sotrudniki where telephon='"+number+"'";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        String vxod = "";
        while (result.next()){
            vxod = result.getString(1);
        }
        return vxod;
    }

    public Integer getRole(int number) throws SQLException, ClassNotFoundException {
        String sql ="SELECT role.id_role FROM sotrudniki, role where role.id_role =  sotrudniki.id_role and sotrudniki.telephon = '"+number+"'";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        Integer vxod = 0;
        while (result.next()){
            vxod = result.getInt(1);
        }
        return vxod;
    }

    public String getRole0(int number) throws SQLException, ClassNotFoundException {
        String sql ="SELECT role.role FROM dem.sotrudniki, dem.role where role.id_role =  sotrudniki.id_role and sotrudniki.telephon = '"+number+"'";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        String vxod = "";
        while (result.next()){
            vxod = result.getString(1);
        }
        return vxod;
    }


    public ArrayList<String> getList() throws SQLException, ClassNotFoundException {
        String sql ="SELECT kkal, name, opisanie FROM products;";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        ArrayList<String> product = new ArrayList<>();
        while (result.next()){
            product.add(result.getString(1));
            product.add(result.getString(2));
            product.add(result.getString(3));
        }
        return product;
    }


    public String getType(int i) throws SQLException, ClassNotFoundException {
        String sql ="SELECT typeproduct.Type FROM typeproduct, products where typeproduct.ID=products.type and products.idproducts = '"+i+"';";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        String product = "";
        while (result.next()){
            product = result.getString(1);
        }
        System.out.println(product);
        return product;
    }


    public void getInsert(Integer id, String name, String kkal, String opisanie, String type) throws SQLException, ClassNotFoundException {
        int typeId = 0;
        if (type.equals("Фрукт")){
            typeId = 1;
        }else if (type.equals("Шоколадка")){
            typeId = 2;
        }else if (type.equals("Булочка")){
            typeId = 3;
        }
        String sql1 = "INSERT INTO `products` (`idproducts`, `name`, `kkal`, `opisanie`, `type`) VALUES (?, ?, ?, ?, ?);";
        PreparedStatement pst;
        pst = dbConn.prepareStatement(sql1);
        pst.setInt(1, id);
        pst.setString(2, name);
        pst.setString(3, kkal);
        pst.setString(4, opisanie);
        pst.setInt(5, typeId);
        pst.executeUpdate();

    }
    public Integer getMaxID() throws SQLException, ClassNotFoundException {
        String sql ="SELECT max(idproducts) FROM products;";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        Integer product = null;
        while (result.next()){
            product = result.getInt(1)+1;
        }
        return product;
    }

    public void getUpdate(Integer id, String name, String kkal, String opisanie) throws SQLException, ClassNotFoundException {

        String sql1 = "UPDATE `products` SET `name` = '"+name+"', `kkal` = '"+kkal+"', `opisanie` = '"+opisanie+"' WHERE (`idproducts` = '"+id+"');";
        Statement statement = getDBConn().createStatement();
        statement.executeUpdate(sql1);



    }

    public void getDelete(Integer id) throws SQLException, ClassNotFoundException {

        String sql1 = "DELETE FROM `products` WHERE (`idproducts` = '"+id+"');";
        Statement statement = getDBConn().createStatement();
        statement.executeUpdate(sql1);
    }


}
