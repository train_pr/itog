package com.example.practica;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


import static java.util.concurrent.TimeUnit.SECONDS;

public class AvtorizationController implements Initializable {

    @FXML
    private Button reserch;

    @FXML
    private TextField password;
    @FXML
    private  TextField code;
    @FXML
    private Button vxod;
    @FXML
    public Label errors;

    @FXML
    private TextField number;

    ConnectDB db = new ConnectDB();

    static int role;
    //отсчет времени после закрытия окошка с кодом
    int countdownStarter = 10;

    //в эту переменную будет записан код
    String builder = new String();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //добавляем картинку на кнопку
        ImageView imageView = new ImageView(new Image("obnovlenie.jpg",20, 20, false, true ));
        reserch.setGraphic(imageView);
    }
    //KeyEvent отслеживает все действия в TextField
    @FXML
    private void onPassword(KeyEvent keyEvent) throws SQLException, ClassNotFoundException {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            if ((db.getPassword(number.getText()).equals(password.getText()))) {
                AlertDialog();
            } else {
                errors.setText("Введен неверный пароль");
            }

        }
    }
    //запуск alert dialog
    private void AlertDialog(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        final char[] captchaSymbols = {
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        };


        //формируем код
        Random random = new Random();
        for (int i = 0; i < 8; i++){
            builder += captchaSymbols[random.nextInt(captchaSymbols.length)];
            System.out.println(i);
        }
        System.out.println(builder);
        //при нажании на кнопку OK будет запущен таймер
        alert.setHeaderText(String.valueOf(builder));
        Optional<ButtonType> op = alert.showAndWait();
        if(op.get() == ButtonType.OK){
            code.setDisable(false);
            vxod.setDisable(false);
            reserch.setDisable(false);
            final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

            final Runnable runnable = new Runnable() {


                public void run() {

                    System.out.println(countdownStarter);
                    countdownStarter--;

                    if (countdownStarter < 0) {
                        scheduler.shutdown();
                    }
                }
            };
            scheduler.scheduleAtFixedRate(runnable, 0, 1, SECONDS);
        }
    }

    public static String Name = "";
    //при верно введенном коде и нет
    public void onCodeAvtorization(ActionEvent actionEvent) throws SQLException, ClassNotFoundException, IOException {
        if(builder.toString().equals(code.getText()) && countdownStarter > 0){
            role = Integer.parseInt(number.getText());
            Stage primaryStage = new Stage();
            Name = db.getRole0(Integer.parseInt(number.getText()));
            System.out.println(Name);
            Parent root = FXMLLoader.load(getClass().getResource("MainForm.fxml"));
            primaryStage.setScene(new Scene(root, 600, 400));
            primaryStage.getIcons().add(new Image("ico.jpg"));
            primaryStage.setMaxHeight(500);
            primaryStage.setMinHeight(400);
            primaryStage.setMaxWidth(700);
            primaryStage.setMinWidth(600);
            primaryStage.setTitle("Продукты 24/7");
            primaryStage.show();
            Stage stage = (Stage) vxod.getScene().getWindow();
            stage.close();


        }else if (builder.toString().equals(code.getText()) && countdownStarter <= 0){
            errors.setText("Истекло время кода");
        }else {
            errors.setText("Неверно введен код");
        }
    }

    //просмотр, есть ли номер в бд
    @FXML
    private void onNumber(KeyEvent keyEvent) throws ClassNotFoundException, SQLException {
        if(keyEvent.getCode() == KeyCode.ENTER){
            String textNumber = number.getText();
            Integer proverca = db.getSotrydnic(textNumber);

            if(proverca == 1){
            password.setDisable(false);
            errors.setText(" ");

            }else{
                errors.setText("Такого номера нет в базе данных.");
            }
        }


    }
    //формирование нового кода
    public void onNewCode(ActionEvent actionEvent){
        countdownStarter = 10;
        builder = "";
        AlertDialog();
    }
    //очистка всех TextField
    public void onOtmena(ActionEvent actionEvent){
        code.clear();
        number.clear();
        password.clear();
        code.setDisable(true);
        password.setDisable(true);
        vxod.setDisable(true);
        reserch.setDisable(true);
    }

}